﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Toolbox.Networking
{
    public class Client : MonoBehaviour
    {
        public ClientList mConnectedClients;
        public int mAssignedID;
        protected ConnectionInformation mConnectionInfo;
        public NetworkQueue mNetQueue;
        public Transform mPlayerEntity;
        public Transform mPlayerRotation;
        private float Timer;
        public float MessageRateMS;
        private float PrevTime;
        public Text mClientTimer;
        public string mRecievedMSG;
        void Start()
        {
            PrevTime = -1.0f;
            mClientTimer = GameObject.Find("ClientTimer").GetComponent<Text>();
            mConnectionInfo = ScriptableObject.CreateInstance("ConnectionInformation") as ConnectionInformation;
            mConnectedClients = ScriptableObject.CreateInstance("ClientList") as ClientList;
            Initialise();
        }
        void Update()
        {
            Timer += Time.deltaTime;
            mClientTimer.text = Timer.ToString();
            if (mPlayerEntity == null)
            {
                if (GameObject.Find("Humanoid") != null)
                {
                    mPlayerEntity = GameObject.Find("Humanoid").transform;
                }
            }
            if (mPlayerRotation == null)
            {
                if (GameObject.Find("Humanoid") != null)
                {
                    mPlayerRotation = GameObject.Find("Humanoid").transform;
                }
            }
            if (mPlayerEntity != null)
            {
                SendEntityData(mConnectionInfo.GetConnectionId());
            }
            if (!mConnectionInfo.GetRunningState())
            {
                return;
            }
            int recHostId;
            int connectionId;
            int channelId;
            byte[] recBuffer = new byte[1024];
            int bufferSize = 1024;
            int dataSize;
            byte error;
            NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
            switch (recData)
            {
                case NetworkEventType.Nothing:         //1
                    break;
                case NetworkEventType.ConnectEvent:    //2
                    break;
                case NetworkEventType.DataEvent:       //3
                    HandleData(recHostId, connectionId, channelId, recBuffer, dataSize);
                    break;
                case NetworkEventType.DisconnectEvent: //4
                    break;
            }
        }
        public void Initialise()
        {
            mConnectedClients.Initialise();
            mConnectionInfo.SetIpAddress("");
            mConnectionInfo.SetPort(65001);
            mConnectionInfo.SetMaxConnections(10);

            NetworkTransport.Init();
            ConnectionConfig tConfiguration = new ConnectionConfig();

            mConnectionInfo.AddChannel("ReliableChannel", tConfiguration.AddChannel(QosType.Reliable));
            mConnectionInfo.AddChannel("UnreliableChannel", tConfiguration.AddChannel(QosType.Unreliable));

            HostTopology tTopology = new HostTopology(tConfiguration, mConnectionInfo.GetMaxConnections());

            mConnectionInfo.AddHostId(NetworkTransport.AddHost(tTopology, mConnectionInfo.GetPort()));
            mConnectionInfo.ToggleRunningState();
        }
        public void Connect()
        {
            string tIp = GameObject.Find("AddressInput").transform.GetChild(2).GetComponent<Text>().text;
            int tPort = -1;
            int.TryParse(GameObject.Find("PortInput").transform.GetChild(2).GetComponent<Text>().text, out tPort);
            mConnectionInfo.SetConnectionId(NetworkTransport.Connect(mConnectionInfo.GetHostId(), tIp, tPort, 0, out mConnectionInfo.mError));
            mConnectionInfo.SetConnectionTime(Time.time);
        }
        public void ConnectToLocalHost()
        {
            mConnectionInfo.SetConnectionId(NetworkTransport.Connect(mConnectionInfo.GetHostId(), "127.0.0.1", 65000, 0, out mConnectionInfo.mError));
            mConnectionInfo.SetConnectionTime(Time.time);
        }
        public void Disconnect()
        {
            NetworkTransport.Disconnect(mConnectionInfo.GetHostId(), mConnectionInfo.GetConnectionId(), out mConnectionInfo.mError);
        }
        public void Destroy()
        {
            NetworkTransport.Shutdown();
            Destroy(GameObject.Find("Networking"));
        }
        public void ToggleClientMenuDebugging()
        {
            GameObject.Find("Canvas").transform.GetChild(1).gameObject.SetActive(!GameObject.Find("Canvas").transform.GetChild(1).gameObject.activeSelf);
            GameObject.Find("Canvas").transform.GetChild(2).gameObject.SetActive(!GameObject.Find("Canvas").transform.GetChild(2).gameObject.activeSelf);
        }
        private void HandleData(int recHostId, int connectionId, int channelId, byte[] recBuffer, int dataSize)
        {
            string iMSG = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
            string[] rMSG = iMSG.Split(new char[] { '|' });
            if (rMSG[0].ToString() == "RQST")
            {
                if (rMSG[1].ToString() == "HNDSHK")
                {
                    if (Handshake(connectionId))
                    {
                        GameObject.Find("ClientMenu").gameObject.SetActive(false);
                        //GameObject.Find("GameInterface").gameObject.SetActive(true);
                    }
                }
                if (rMSG[1].ToString() == "CLIENT")
                {
                    //Handle requests made by the server
                    print("RQST Recieved");
                }
            }
            if (rMSG[0].ToString() == "INST")
            {
                if (rMSG[1].ToString() == "ID")
                {
                    //Hard coding player details untill data driven system is implemented
                    int.TryParse(rMSG[2].ToString(), out mAssignedID);
                    mNetQueue.mPlayerRef.Add(mAssignedID, CreateLocalEntity());
                }
                for (int i = 1; i < rMSG.Length; i += 2)
                {
                    if (rMSG[i].ToString() == "NEWCLIENT")
                    {
                        string[] tDATA = rMSG[i + 1].Split(new char[] { ',' });
                        GameObject t = Instantiate(Resources.Load("Prefabs/ClientEntity")) as GameObject;
                        t.AddComponent<NetworkMovement>();
                        int tID;
                        int.TryParse(tDATA[1].ToString(), out tID);
                        t.name = tDATA[2].ToString();
                        if (!mNetQueue.mPlayerRef.ContainsKey(tID))
                        {
                            mNetQueue.mPlayerRef.Add(tID, t);
                            print("Player: " + tID + " Created");
                        }
                        else
                        {
                            Destroy(t);
                        }
                    }
                }
                if (rMSG[1].ToString() == "CLIENT")
                {
                    if (rMSG[2].ToString() != mConnectionInfo.GetConnectionId().ToString())
                    {
                        int ClientID = -1;
                        int.TryParse(rMSG[2].ToString(), out ClientID);
                        mNetQueue.InterrogateRQST(ClientID, iMSG);
                    }
                }
            }
            if (rMSG[0].ToString() == "MSG")
            {
                GameObject tRmsg = GameObject.Find("RecieveMSG");
                if (tRmsg != null)
                {
                    tRmsg.GetComponent<Text>().text = rMSG[1].ToString();
                }
            }
        }
        public GameObject CreateLocalEntity()
        {
            GameObject t = Instantiate(Resources.Load("Prefabs/ClientEntity")) as GameObject;
            t.AddComponent<Base>();
            int terrain = 8;
            int items = 12;
            int layermask1 = 1 << terrain;
            int layermask2 = 1 << items;
            int FinalMask = layermask1 | layermask2;
            t.GetComponent<Base>().mTargetLayers = FinalMask;
            t.AddComponent<CapsuleCollider>();
            t.GetComponent<CapsuleCollider>().height = 2.0f;
            t.GetComponent<CapsuleCollider>().center = new Vector3(GameObject.Find("Humanoid").transform.position.x, 1.0f, GameObject.Find("Humanoid").transform.position.z);
            t.AddComponent<PlayerInventory>();
            t.AddComponent<Movement>();
            t.GetComponent<Movement>().mEntityRoot = GameObject.Find("Humanoid").transform;
            t.GetComponent<Movement>().mLookSensitivity = 4;
            t.GetComponent<Movement>().mSpeed = 3;
            t.GetComponent<Movement>().mNetQueue = GameObject.Find("Networking").GetComponent<NetworkQueue>();
            GameObject gimbal = GameObject.Find("CameraGimbal");
            gimbal.transform.SetParent(t.transform);
            Vector3 tVec = t.GetComponent<Movement>().mEntityRoot.position;
            gimbal.transform.eulerAngles = new Vector3(tVec.x, gimbal.transform.localPosition.y, tVec.y);
            t.GetComponent<Movement>().mCameraRotationRoot = gimbal.transform;
            t.name = "LocalPlayer";
            return t;
        }
        public bool SendData(string prepend, string iData, int iConnectionID)
        {
            string tData = prepend + "|" + iData;
            byte[] DATA = Encoding.Unicode.GetBytes(tData);
            if (NetworkTransport.Send(mConnectionInfo.GetHostId(),
                    iConnectionID,
                    mConnectionInfo.GetChannel("ReliableChannel"),
                    DATA,
                    tData.Length * sizeof(char),
                    out mConnectionInfo.mError))
            {
                return true;
            }
            return false;
        }
        public void SendTextInputField()
        {
            string tMsg = GameObject.Find("SendMSG").transform.GetChild(2).GetComponent<Text>().text;
            SendData("MSG", tMsg, mConnectionInfo.GetConnectionId());
        }
        public void SendEntityData(int iConnectionID)
        {
            print(PrevTime.ToString());
            if (Timer >= PrevTime + MessageRateMS);
            {
                PrevTime = Timer;
                print(PrevTime.ToString());
                string tPos;
                string tRot;
                tPos = "MOV|" + mPlayerEntity.position.x.ToString() + "," + mPlayerEntity.position.y.ToString() + "," + mPlayerEntity.position.z.ToString();
                tRot = "ROT|" + mPlayerRotation.localRotation.eulerAngles.x + "," + mPlayerRotation.localRotation.eulerAngles.y + "," + mPlayerRotation.localRotation.eulerAngles.z;
                string tData = tPos + "|" + tRot;
                SendData("RQST", tData, iConnectionID);
            }
        }
        private bool Handshake(int iConnectionID)
        {
            string tQueryName = "KHSDNH";
            if (SendData("HNDSHK", tQueryName, iConnectionID))
            {
                string iRSPNS = "";
                string[] rRSPNS = new string[1];
                while (iRSPNS == "")
                {
                    int recHostId;
                    int connectionId;
                    int channelId;
                    byte[] recBuffer = new byte[1024];
                    int bufferSize = 1024;
                    int dataSize;
                    byte error;
                    NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
                    switch (recData)
                    {
                        case NetworkEventType.DataEvent:
                            iRSPNS = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
                            rRSPNS = iRSPNS.Split(new char[] { '|' });
                            break;
                    }
                    if (false)
                    {
                        print("Handshake Timeout");
                        return false;
                    }
                }
                if (rRSPNS[0].ToString() == "HNDSHK")
                {
                    if (rRSPNS[1].ToString() == "HNDKHS")
                    {
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                Debug.Log("Unable To Establish Handshake");
                return false;
            }
        }
    }
}