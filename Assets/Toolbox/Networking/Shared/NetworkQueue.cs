﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkQueue : MonoBehaviour
{
    public Dictionary<int, GameObject> mPlayerRef;
    string[] testString;
    // Use this for initialization
    void Start()
    {
        mPlayerRef = new Dictionary<int, GameObject>();
    }
    public string InterrogateRQST(int iPlayerID, string iMsg)
    {
        string output = "";
        testString = iMsg.Split(new char[] { '|' });
        for (int i = 1; i < testString.Length; i++)
        {
            if (testString[i].ToString() == "MOV")
            {
                output += "MOV|";
                string[] xzy = testString[i + 1].Split(new char[] { ',' });
                float x, y, z;
                float.TryParse(xzy[0].ToString(), out x);
                float.TryParse(xzy[1].ToString(), out y);
                float.TryParse(xzy[2].ToString(), out z);
                mPlayerRef[iPlayerID].GetComponent<NetworkMovement>().mDestination = new Vector3(x, y, z);
                i++;
            }
            if (testString[i].ToString() == "ROT")
            {
                output += "ROT|";
                string[] xzy = testString[i + 1].Split(new char[] { ',' });
                float x, y, z;
                float.TryParse(xzy[0].ToString(), out x);
                float.TryParse(xzy[1].ToString(), out y);
                float.TryParse(xzy[2].ToString(), out z);
                mPlayerRef[iPlayerID].GetComponent<NetworkMovement>().mRotation = Quaternion.Euler(x, y, z);
                i++;
            }
        }
        output = output.Trim('|');
        return output;
    }
}
