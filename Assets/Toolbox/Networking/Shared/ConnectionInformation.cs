﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolbox.Networking
{
    public class ConnectionInformation : ScriptableObject
    {
        private int mMaxConnections;
        private int mPort;
        private int mWebPort;
        private string mAddress;
        private int mHostId;
        private int mWebHostId;
        private Dictionary<string, int> mChannels;
        private int mConnectionId;
        private float mConnectionTime;
        private bool mRunning = false;
        public byte mError;

        public bool SetMaxConnections(int iConnections)
        {
            if (iConnections > 0)
            {
                mMaxConnections = iConnections;
                return true;

            }
            return false;
        }
        public int GetMaxConnections()
        {
            return mMaxConnections;
        }
        public bool SetPort(int iPort)
        {
            if (iPort >= 0 && iPort <= 65535)
            {
                mPort = iPort;
                return true;
            }
            return false;
        }
        public bool SetPort(string iPort)
        {
            int castInt;
            if (int.TryParse(iPort, out castInt))
            {
                if (castInt >= 0 && castInt <= 65535)
                {
                    mPort = castInt;
                    return true;
                }
            }
            return false;
        }
        public int GetPort()
        {
            return mPort;
        }
        public bool SetWebPort(int iPort)
        {
            if (iPort >= 0 && iPort <= 65535)
            {
                mWebPort = iPort;
                return true;
            }
            return false;
        }
        public bool SetWebPort(string iPort)
        {
            int castInt;
            if (int.TryParse(iPort, out castInt))
            {
                if (castInt >= 0 && castInt <= 65535)
                {
                    mWebPort = castInt;
                    return true;
                }
            }
            return false;
        }
        public int GetWebPort()
        {
            return mWebPort;
        }
        public void SetIpAddress(string iAddress)
        {
            mAddress = iAddress;
        }
        public string GetIpAddress()
        {
            return mAddress;
        }

        public void AddChannel(string iName, int iChannelType)
        {
            if (mChannels == null)
            {
                mChannels = new Dictionary<string, int>();
            }
            mChannels.Add(iName, iChannelType);
        }
        public void SetChannel(string iName, int iChannelType)
        {
            if (mChannels.ContainsKey(iName))
            {
                Debug.Log("Channel: " + iName + " Type changing from: " + mChannels[iName]);
                mChannels[iName] = iChannelType;
                Debug.Log(" To: " + iChannelType);
            }
        }
        public int GetChannel(string iName)
        {
            if (mChannels.ContainsKey(iName))
            {
                return mChannels[iName];
            }
            Debug.Log("No Channel by that name");
            return -1;
        }

        public void AddHostId(int iId)
        {
            mHostId = iId;
        }
        public int GetHostId()
        {
            return mHostId;
        }
        public void AddWebHostId(int iId)
        {
            mWebHostId = iId;
        }
        public int GetWebHostId()
        {
            return mWebHostId;
        }

        public void SetConnectionId(int iId)
        {
            mConnectionId = iId;
        }
        public int GetConnectionId()
        {
            return mConnectionId;
        }
        public void ToggleRunningState()
        {
            mRunning = !mRunning;
        }
        public bool GetRunningState()
        {
            return mRunning;
        }
        public void SetConnectionTime(float iTime)
        {
            mConnectionTime = iTime;
        }
        public float GetConnectionTime()
        {
            return mConnectionTime;
        }
    }


}
