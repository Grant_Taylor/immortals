﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientReference
{
    public int mConnectionID;
    public string mName;
    public int mServerAssignedID;
    public string mEntityReference;
}

public class ClientList : ScriptableObject
{
	public List<ClientReference> mClientList;
    public void Initialise()
    {
		mClientList = new List<ClientReference>();
    }
}
