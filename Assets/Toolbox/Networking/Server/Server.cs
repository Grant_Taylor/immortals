﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace Toolbox.Networking
{
    public class Server : MonoBehaviour
    {
        public ClientList mConnectedClients;
        public ConnectionInformation mConnectionInfo;
        public NetworkQueue mNetQueue;
        private float Timer;
        public Text mServerTimer;
        void Start()
        {
            mServerTimer = GameObject.Find("ServerTimer").GetComponent<Text>();
            mConnectionInfo = ScriptableObject.CreateInstance("ConnectionInformation") as ConnectionInformation;
            mConnectedClients = ScriptableObject.CreateInstance("ClientList") as ClientList;
            Initialise();
        }
        void Update()
        {
            Timer += Time.deltaTime;
            mServerTimer.text = Timer.ToString();
            if (!mConnectionInfo.GetRunningState())
            {
                return;
            }
            int recHostId;
            int connectionId;
            int channelId;
            byte[] recBuffer = new byte[1024];
            int bufferSize = 1024;
            int dataSize;
            byte error;
            NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
            switch (recData)
            {
                case NetworkEventType.Nothing:
                    break;
                case NetworkEventType.ConnectEvent:
                    InterrogateConnection(connectionId);
                    //send client creation instruction once client has entered the corect scene
                    //InitialiseClientEntities();
                    break;
                case NetworkEventType.DataEvent:
                    HandleData(recHostId, connectionId, channelId, recBuffer, dataSize);
                    break;
                case NetworkEventType.DisconnectEvent:
                    Debug.Log("Player: " + connectionId + " has disconnected.");
                    //remove entity under this ID
                    break;
            }
        }
        public void Initialise()
        {
            mConnectedClients.Initialise();
            //ConnectionInfo.SetIpAddress("");
            mConnectionInfo.SetPort(65000);
            mConnectionInfo.SetWebPort(65005);
            mConnectionInfo.SetMaxConnections(16);

            NetworkTransport.Init();
            ConnectionConfig tConfiguration = new ConnectionConfig();

            mConnectionInfo.AddChannel("ReliableChannel", tConfiguration.AddChannel(QosType.Reliable));
            mConnectionInfo.AddChannel("UnreliableChannel", tConfiguration.AddChannel(QosType.Unreliable));

            HostTopology tTopology = new HostTopology(tConfiguration, mConnectionInfo.GetMaxConnections());

            mConnectionInfo.AddHostId(NetworkTransport.AddHost(tTopology, mConnectionInfo.GetPort()));
            mConnectionInfo.AddWebHostId(NetworkTransport.AddWebsocketHost(tTopology, mConnectionInfo.GetWebPort()));
            mConnectionInfo.ToggleRunningState();
        }
        private void InterrogateConnection(int iConnectionID)
        {
            if (Handshake(iConnectionID))
            {
                if (!mNetQueue.mPlayerRef.ContainsKey(iConnectionID))
                {
                    GameObject t = Instantiate(Resources.Load("Prefabs/ClientEntity")) as GameObject;
                    t.AddComponent<NetworkMovement>();
                    t.name = "Player " + iConnectionID.ToString();
                    mNetQueue.mPlayerRef.Add(iConnectionID, t);
                    print(iConnectionID + " Added to mConnectectClients");
                    ClientReference newClient = new ClientReference();
                    newClient.mConnectionID = iConnectionID;
                    newClient.mName = "Player" + " " + iConnectionID.ToString();
                    newClient.mServerAssignedID = iConnectionID;
                    newClient.mEntityReference = "ENTITY" + "," + iConnectionID.ToString();
                    mConnectedClients.mClientList.Add(newClient);
                    SendData("INST", "ID|" + newClient.mServerAssignedID, iConnectionID);
                }
            }
        }
        private bool Handshake(int iConnectionID)
        {
            string tQueryName = "RQST|HNDSHK";
            byte[] oRQST = Encoding.Unicode.GetBytes(tQueryName);
            if (NetworkTransport.Send(mConnectionInfo.GetHostId(),
                    iConnectionID,
                    mConnectionInfo.GetChannel("ReliableChannel"),
                    oRQST,
                    tQueryName.Length * sizeof(char),
                    out mConnectionInfo.mError))
            {
                string iRSPNS = "";
                string[] rRSPNS = new string[1];
                while (iRSPNS == "")
                {
                    int recHostId;
                    int connectionId;
                    int channelId;
                    byte[] recBuffer = new byte[1024];
                    int bufferSize = 1024;
                    int dataSize;
                    byte error;
                    NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
                    switch (recData)
                    {
                        case NetworkEventType.DataEvent:
                            iRSPNS = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
                            rRSPNS = iRSPNS.Split(new char[] { '|' });
                            break;
                    }
                }
                if (rRSPNS[0].ToString() == "HNDSHK")
                {
                    if (rRSPNS[1].ToString() == "KHSDNH")
                    {
                        string tResponse = "HNDSHK|HNDKHS";
                        byte[] oRSPNS = Encoding.Unicode.GetBytes(tResponse);
                        NetworkTransport.Send(mConnectionInfo.GetHostId(),
                                iConnectionID,
                                mConnectionInfo.GetChannel("ReliableChannel"),
                                oRSPNS,
                                tResponse.Length * sizeof(char),
                                out mConnectionInfo.mError);
                        return true;
                    }
                }
                else
                {
                    //Handshake token incorrect
                    //add timout loop
                    return false;
                }
            }
            else
            {
                //Connection settings incorrect
                //add timeout loop
                return false;
            }
            //Critical fail
            return false;
        }
        public bool SendData(string prepend, string iData, int iConnectionID)
        {
            string tData = prepend + "|" + iData;
            byte[] DATA = Encoding.Unicode.GetBytes(tData);
            if (NetworkTransport.Send(mConnectionInfo.GetHostId(),
                    iConnectionID,
                    mConnectionInfo.GetChannel("ReliableChannel"),
                    DATA,
                    tData.Length * sizeof(char),
                    out mConnectionInfo.mError))
            {
                return true;
            }
            return false;
        }
        private void RelayDATA(string prepend, string iMSG, int iConnectionID)
        {
            prepend += "|CLIENT|" + iConnectionID.ToString() + "|";
            foreach (ClientReference c in mConnectedClients.mClientList)
            {
                if (c.mConnectionID != iConnectionID)
                {
                    SendData(prepend, iMSG, c.mConnectionID);
                }
            }
        }
        private void HandleData(int recHostId, int connectionId, int channelId, byte[] recBuffer, int dataSize)
        {
            string iMSG = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
            string[] rMSG = iMSG.Split(new char[] { '|' });
            if (rMSG[0].ToString() == "RQST")
            {
                string rRQSTS = mNetQueue.InterrogateRQST(connectionId, iMSG);
                RelayDATA("INST", iMSG, connectionId);
            }
            if (rMSG[0].ToString() == "MSG")
            {
                GameObject tRmsg = GameObject.Find("RecievedMSG");
                if (tRmsg != null)
                {
                    RelayDATA(rMSG[0].ToString(), rMSG[1].ToString(), connectionId);
                    tRmsg.GetComponent<Text>().text = rMSG[1].ToString();
                }
            }
        }
        private void InitialiseClientEntities()
        {
            string tClientInfo = "";
            foreach (ClientReference c in mConnectedClients.mClientList)
            {
                tClientInfo += "NEWCLIENT|" + c.mConnectionID + "," + c.mServerAssignedID + "," + c.mName + '|';
            }
            tClientInfo = tClientInfo.Trim('|');
            foreach (ClientReference c in mConnectedClients.mClientList)
            {
                SendData("INST", tClientInfo, c.mConnectionID);
            }
        }
    }
}