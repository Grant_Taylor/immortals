﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : Interactable
{
	public override void Interact()
	{
		mPlayer.gameObject.GetComponent<PlayerInventory>().mItemList.Add(this.name);
		Destroy(this.gameObject);
	}
}
