﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayAndNight : MonoBehaviour
{
	public float mTimeScale = 1.0f;
    void Update()
    {
        transform.Rotate(Vector3.up, mTimeScale * Time.deltaTime);
    }
}
