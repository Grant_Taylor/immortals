﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float mRadius = 1f;
	public Transform mInteractionTransform;

	bool mIsFocus = false;
	bool mHasInteracted = false;
	protected Transform mPlayer;

	public virtual void Interact()
	{
		Debug.Log("Interacting with: "+ mPlayer.transform);
	}

	void Update()
	{
		if(mIsFocus && !mHasInteracted)
		{
			float tDistance = Vector3.Distance(mPlayer.position, mInteractionTransform.position);
			if(tDistance <= mRadius)
			{
				Interact();
				mHasInteracted = true;
			}
		}
	}
	public void OnFocussed(Transform playerTransform)
	{
		mIsFocus = true;
		mPlayer = playerTransform;
		mHasInteracted = false;
	}

	public void OnDefocussed()
	{
		mIsFocus = false;
		mPlayer = null;
		mHasInteracted = false;
	}

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(mInteractionTransform.position, mRadius);
    }

}
