﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkMovement : MonoBehaviour
{
    public NetworkQueue mNetQueue;
    public Vector3 mDestination;
    public Quaternion mRotation;
    // Use this for initialization
    void Start()
    {
		mDestination = Vector3.zero;
		mRotation = Quaternion.identity;
    }

    // Update is called once per frame
    void Update()
    {
		transform.position = Vector3.Lerp(transform.position, mDestination, 3 * Time.deltaTime);
		transform.rotation = Quaternion.Slerp(transform.rotation, mRotation, 4 * Time.deltaTime);
    }
}
