﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Base : MonoBehaviour
{
    public Interactable FocusItem;
    public RaycastMouse MouseTarget;

    public LayerMask mTargetLayers;
    void Start()
    {
        MouseTarget = this.gameObject.AddComponent<RaycastMouse>();
    }

    void Update()
    {
        if (FocusItem != null)
        {
            //add register of fucussed item so player can see information about the chosen target
        }
        if (Input.GetMouseButton(0))
        {
            //assume player is attemptint to focus a new item so defocuss previous item
            RemoveFocus();

            //attempt to assign target interactable component
            if (MouseTarget.ReturnObjectUnderMouse(mTargetLayers).collider != null)
            {
                Interactable interactable = MouseTarget.ReturnObjectUnderMouse(mTargetLayers).collider.GetComponent<Interactable>();
                {
                    //Checking to see if target is interactible
                    if (interactable != null)
                    {
                        //Set Focus item
                        SetFocus(interactable);
                        return;
                    }
                }

            }
        }
        if (Input.GetMouseButton(1))
        {
            //context menu
        }
    }
    void SetFocus(Interactable newFocus)
    {
        if (newFocus != FocusItem)
        {
            if (FocusItem != null)
            {
                FocusItem.OnDefocussed();
            }
            FocusItem = newFocus;
        }
        FocusItem.OnFocussed(transform);
    }
    void RemoveFocus()
    {
        if (FocusItem != null)
        {
            FocusItem.OnDefocussed();
        }
        FocusItem = null;
    }
}
