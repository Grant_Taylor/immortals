﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public NetworkQueue mNetQueue;
    public float mSpeed;

    public float mLookSensitivity;
    public Transform mCameraRotationRoot;
    public Transform mEntityRoot;
    private float mRotationX = 0.0f;
    public bool mInvertMouse;
    public float mRotationY = 0;

    public bool mOnlinePlayer;


    void Start()
    {
    }

    void Update()
    {
        MoveEntity();
        if (Cursor.lockState == CursorLockMode.Locked)
        {
            RotateCamera();
        }
    }
    private void MoveEntity()
    {
        //Test for input
        if (Input.GetAxisRaw("Horizontal") != 0.0f || Input.GetAxisRaw("Vertical") != 0.0f)
        {
            //calculate new position from normalised input vector multiplied by a speed value that is clamped by delta time
            transform.position += (((mEntityRoot.transform.right * Input.GetAxis("Horizontal")) + (mEntityRoot.transform.forward * Input.GetAxis("Vertical")).normalized) * mSpeed) * Time.deltaTime;
            RotateTowardsLookDirection(Camera.main.transform.forward);
            if (mOnlinePlayer)
            {
                //report position and velocity to server
            }

        }
    }
    private void RotateCamera()
    {
        //Rotate camera gimbal based on mouse input
        mCameraRotationRoot.localRotation *= Quaternion.Euler(new Vector3(0, (Input.GetAxisRaw("Mouse X") * mLookSensitivity), 0));
        //Get the y axis input from the mouse (invert based on boolean value)
        mRotationX += mInvertMouse ? Input.GetAxisRaw("Mouse Y") * mLookSensitivity : -Input.GetAxisRaw("Mouse Y") * mLookSensitivity;
        //clamp the y axis to lock up and down look direction
        mRotationX = Mathf.Clamp(mRotationX, -80.0f, 80.0f);
        //rotate camera around the player according to constraints emposed by the clamp
        mCameraRotationRoot.localRotation = Quaternion.Euler(new Vector3((mRotationX), mCameraRotationRoot.localRotation.eulerAngles.y, 0));
    }
    private void RotateTowardsLookDirection(Vector3 iDir)
    {
        //apply desired direction
        mEntityRoot.localRotation = Quaternion.Euler(mEntityRoot.localRotation.eulerAngles.x, mCameraRotationRoot.localRotation.eulerAngles.y, mEntityRoot.localRotation.eulerAngles.z);
        if (mOnlinePlayer)
        {
            //report rotation to server
        }
    }
}