﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleCursor : MonoBehaviour
{
    private bool mMouseToggle;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleCursorState();
        }
    }
    public void ToggleCursorState()
    {
        mMouseToggle = !mMouseToggle;
        if (mMouseToggle)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
			Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
    public bool GetCursorState()
    {
        return mMouseToggle;
    }
}
