﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastMouse : MonoBehaviour
{
    Camera mCam;

    void Start()
    {
        mCam = Camera.main;
    }
    void Update()
    {
        ReturnObjectUnderMouse();
    }

    public RaycastHit ReturnObjectUnderMouse()
    {
        Ray ray = mCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, 100))
        {
            return hit;
        }
        return hit;
    }
    public RaycastHit ReturnObjectUnderMouse(LayerMask iLayers)
    {
        Ray ray = mCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, 100, iLayers))
        {
            return hit;
        }
        return hit;
    }
    public Ray ReturnMouseDirection()
    {
        Ray ray = mCam.ScreenPointToRay(Input.mousePosition);
        return ray;
    }
}
