﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Toolbox.Networking
{
    public class QuitApplication : MonoBehaviour
    {
        public void Quit()
        {
#if UNITY_STANDALONE
            Application.Quit();
#endif
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }
        public void Disconnect()
        {
            print("Disconnecting");
            GameObject.Find("Networking").GetComponent<Client>().Disconnect();
            GameObject.Find("Networking").GetComponent<Client>().Destroy();
            print("Loading new scene");
            SceneManager.LoadScene("Client");

        }
    }
}