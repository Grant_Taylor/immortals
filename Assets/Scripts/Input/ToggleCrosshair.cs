﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleCrosshair : MonoBehaviour
{
    public List<GameObject> MenuItems;

	public GameObject mCrosshair;

    void Start()
    {
        MenuItems = new List<GameObject>();
    }

    void Update()
    {
        if (Cursor.lockState == CursorLockMode.Locked)
        {
            mCrosshair.SetActive(true);
			//rough force closing of menu elements
            for (int x = transform.childCount-1; x >= 1; x--)
            {
                if (transform.GetChild(x).gameObject.activeSelf)
                {
                    transform.GetChild(x).gameObject.SetActive(false);
                }
            }
            transform.GetChild(2).gameObject.SetActive(true);
        }
        else
        {
            mCrosshair.SetActive(false);
        }
    }
}
