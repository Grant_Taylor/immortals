﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestInstructionInput : MonoBehaviour
{
    public GameObject controller;

    void Update()
    {
        if (controller.activeSelf == false)
        {
            controller.SetActive(true);
        }
    }
}
