﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMenu : MonoBehaviour
{
    public ToggleCursor ToggleCursorRef;
    public GameObject mMenu;
    void Start()
    {
        ToggleCursorRef = GetComponent<ToggleCursor>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            ToggleMenu();
        }

    }
    public void ToggleMenu()
    {
        mMenu.SetActive(!mMenu.activeSelf);
        if (ToggleCursorRef.GetCursorState())
        {
            ToggleCursorRef.ToggleCursorState();    
        }

    }
}