﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryMenu : MonoBehaviour
{
    public ToggleCursor ToggleCursorRef;
    public GameObject mMenu;
    // Use this for initialization
    void Start()
    {
        ToggleCursorRef = GetComponent<ToggleCursor>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            ToggleMenu();
        }
    }
    public void ToggleMenu()
    {
        mMenu.SetActive(!mMenu.activeSelf);
        if (ToggleCursorRef.GetCursorState())
        {
            ToggleCursorRef.ToggleCursorState();
        }
    }
}