﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameManager : MonoBehaviour
{
    Toolbox.Networking.NetworkManager mNetManager;
    public bool mOnline;
    private bool mOnce;
    void Start()
    {
        Initialise();
    }
    void Update()
    {
        if (mNetManager != null)
        {
            mNetManager.Listen();
        }
    }
    private void Initialise()
    {
        if (mOnline && !mOnce)
        {
            EnableOnlineMode();
        }
    }
    private void EnableOnlineMode()
    {
        if (!mOnce)
        {
            //Initialisation checks
            mNetManager = this.gameObject.AddComponent<Toolbox.Networking.NetworkManager>();
            mNetManager.InitialiseNetworkManager();
            mNetManager.StartListening();
            mOnce = true;
        }
    }
}
