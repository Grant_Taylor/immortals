﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Toolbox.Networking
{

    public class NetworkManager : MonoBehaviour
    {
        ClientList mConnectedClients;
        ConnectionInformation mConnectionInfo;
        bool mListening;
        public void InitialiseNetworkManager()
        {
            mConnectedClients = ScriptableObject.CreateInstance("ClientList") as ClientList;
            mConnectionInfo = ScriptableObject.CreateInstance("ConnectionInformation") as ConnectionInformation;
            mConnectedClients.Initialise();
            mConnectionInfo.SetIpAddress("");
            mConnectionInfo.SetPort(65000);
            mConnectionInfo.SetWebPort(65005);
            mConnectionInfo.SetMaxConnections(16);

            NetworkTransport.Init();
            ConnectionConfig tConfiguration = new ConnectionConfig();

            mConnectionInfo.AddChannel("ReliableChannel", tConfiguration.AddChannel(QosType.Reliable));
            mConnectionInfo.AddChannel("UnreliableChannel", tConfiguration.AddChannel(QosType.Unreliable));

            HostTopology tTopology = new HostTopology(tConfiguration, mConnectionInfo.GetMaxConnections());

            mConnectionInfo.AddHostId(NetworkTransport.AddHost(tTopology, mConnectionInfo.GetPort()));
            mConnectionInfo.AddWebHostId(NetworkTransport.AddWebsocketHost(tTopology, mConnectionInfo.GetWebPort()));
            mConnectionInfo.ToggleRunningState();
        }
        public void StartListening()
        {
            mListening = true;
        }
        public byte[] Listen()
        {
            int recHostId;
            int connectionId;
            int channelId;
            byte[] recBuffer = new byte[1024];
            int bufferSize = 1024;
            int dataSize;
            byte error;
            NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
            switch (recData)
            {
                case NetworkEventType.Nothing:         //1
                    break;
                case NetworkEventType.ConnectEvent:    //2
                    print("ConnectionID: " + connectionId.ToString() + " Connecting");
                    break;
                case NetworkEventType.DataEvent:       //3
                    string iMSG = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
                    print(iMSG);
                    return recBuffer;
                case NetworkEventType.DisconnectEvent: //4
                    break;
            }
            return new byte[0];
        }
        void Update()
        {
        }
    }
}